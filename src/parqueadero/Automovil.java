/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueadero;

/**
 *
 * @author estudiantes
 */
class Automovil {
    
    private String placa;                             
    private String marca;
    private String color;
    private int cedula;
    private int estadodelAutomovil;

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getEstadodelAutomovil() {
        return estadodelAutomovil;
    }

    public void setEstadodelAutomovil(int estadodelAutomovil) {
        this.estadodelAutomovil = estadodelAutomovil;
    }
        public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

}
